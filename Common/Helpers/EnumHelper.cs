﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Enums;

namespace Common.Helpers
{
    public static class EnumHelpers
    {
        public static IEnumerable<string> GetColorsEnum()
        {
            return Enum.GetNames(typeof(CarColors)).ToList();
        }

        public static string GetStringColor(CarColors color)
        {
            var s = Enum.GetName(typeof(CarColors), color);
            return s;
        }
    }
}
