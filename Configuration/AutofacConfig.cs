﻿using System.Data.Entity;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using NLayerApp.BL.IServices;
using NLayerApp.BL.Services;
using NLayerApp.DLA.Context;
using NLayerApp.DLA.Repository.Contracts;
using NLayerApp.DLA.Repository.EfImplementations;
using NLayerApp.DLA.UnitOfWork;


namespace Configuration
{
    public class AutofacConfig
    {
        public static void Configure(Assembly assembly)
        {
            var builder = new ContainerBuilder();

            builder.RegisterApiControllers(assembly).InstancePerRequest();

            RegisterTypeApp(builder);

            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }


        public static void RegisterTypeApp(ContainerBuilder builder)
        {
            builder.RegisterType<CarContext>().As<DbContext>().InstancePerRequest();

            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();

            builder.RegisterType<CountryRepository>().As<ICountryRepository>().InstancePerRequest();
            builder.RegisterType<ModelRepository>().As<IModelRepository>().InstancePerRequest();
            builder.RegisterType<SalonRepository>().As<ISalonRepository>().InstancePerRequest();
            builder.RegisterType<MarkRepository>().As<IMarkRepository>().InstancePerRequest();
            builder.RegisterType<ManufactureRepository>().As<IManufactureRepository>().InstancePerRequest();
            builder.RegisterType<UserRepository>().As<IUserRepository>().InstancePerRequest();

            builder.RegisterType<SalonInfoServices>().As<ISalonInfoServices>().InstancePerRequest();
            builder.RegisterType<SalonServices>().As<ISalonServices>().InstancePerRequest();

            builder.RegisterType<ManufactureServices>().As<IManufactureServices>().InstancePerRequest();
            builder.RegisterType<CountryServices>().As<ICountryServices>().InstancePerRequest();
            builder.RegisterType<ModelServices>().As<IModelServices>().InstancePerRequest();
            builder.RegisterType<MarkServices>().As<IMarkServices>().InstancePerRequest();
            builder.RegisterType<UserServices>().As<IUserServices>().InstancePerRequest();
        }
    }
}
