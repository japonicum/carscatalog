﻿namespace NLayerApp.DLA.CustomExceptions
{
    public class CustomObjectNotFoundException : System.Exception
    {
        public CustomObjectNotFoundException(string message) : base (message)
        {
            
        }
    }
}
