﻿using System.Data.Entity;
using System.Threading.Tasks;
using NLayerApp.DLA.Repository.Contracts;
// Dapper
using NLayerApp.DLA.Repository.DapperRepository;
// Ado.Net
using NLayerApp.DLA.Repository.AdoNetImplementations;

namespace NLayerApp.DLA.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _context;

        public UnitOfWork(
            DbContext context, 
            IUserRepository userRepository, 
            IMarkRepository markRepository, 
            IModelRepository modelRepository, 
            IManufactureRepository manufactureRepository, 
            ISalonRepository salonRepository,
            ICountryRepository countryRepository)
        {
            _context = context;
            UserRepository = userRepository;
            MarkRepository = markRepository;
            ModelRepository = modelRepository;
            ManufactureRepository = manufactureRepository;
            SalonRepository = salonRepository;
            CountryRepository = countryRepository;
        }

        public IUserRepository UserRepository
        {
            get;
            private set;
        }

        ///////////       ADO.NET    

        //public ISalonRepository SalonRepository
        //{
        //    get { return new AdoSalonRepository(); }
        //}

        //////////      Entity FrameWork  

        public ISalonRepository SalonRepository
        {
            get;
            private set;
        }

        ///////////       Dapper  

        //public ISalonRepository SalonRepository
        //{
        //    get { return new SalonRepositoryDapper(); }
        //}

        public IModelRepository ModelRepository
        {
            get;
            private set;
        }

        public IMarkRepository MarkRepository
        {
            get;
            private set;
        }

        public ICountryRepository CountryRepository
        {
            get;
            private set;
        }

        public IManufactureRepository ManufactureRepository
        {
            get;
            private set;
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
