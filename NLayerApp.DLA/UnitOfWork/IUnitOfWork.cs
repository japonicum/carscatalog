﻿using System.Threading.Tasks;
using NLayerApp.DLA.Repository.Contracts;

namespace NLayerApp.DLA.UnitOfWork
{
    public interface IUnitOfWork
    {
        #region properties

        IUserRepository UserRepository { get;  }

        ISalonRepository SalonRepository { get; }

        IModelRepository ModelRepository { get;   }

        IMarkRepository MarkRepository { get; }

        ICountryRepository CountryRepository { get; }

        IManufactureRepository ManufactureRepository { get; }

        #endregion

        int SaveChanges();

        Task<int> SaveChangesAsync();
    }
}
