﻿using System.Data.Entity.ModelConfiguration;
using NLayerApp.DLA.Entities;

namespace NLayerApp.DLA.Context.Configurations
{
    public class ManufactureConfiguration : EntityTypeConfiguration<Manufacture>
    {
        public ManufactureConfiguration()
        {
            //Primary Key
            HasKey(p => p.ManufactureId);

            //Table Name
            ToTable("Manufacture");

            //Fields Name
            Property(p => p.ManufactureId).HasColumnName("ManufactureId");
            Property(p => p.ManufactureName).HasColumnName("ManufactureName");

            //Rilations
            HasOptional(t => t.Country).
                WithMany(p=>p.Manufactures).
                WillCascadeOnDelete(false);
        }
    }
}
