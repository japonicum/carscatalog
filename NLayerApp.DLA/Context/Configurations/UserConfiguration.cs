﻿using System.Data.Entity.ModelConfiguration;

using NLayerApp.DLA.Entities;

namespace NLayerApp.DLA.Context.Configurations
{
    public class UserConfiguration: EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            HasKey(p => p.UserId);

            ToTable("User");

            Property(u => u.UserName).HasColumnName("UserName");
            Property(u => u.Password).HasColumnName("Password");
        }
    }
}
