﻿using System.Data.Entity.ModelConfiguration;
using NLayerApp.DLA.Entities;

namespace NLayerApp.DLA.Context.Configurations
{
    public class SalonConfiguration : EntityTypeConfiguration<Salon>
    {
        public SalonConfiguration()
        {
            HasKey(salon => salon.SalonId);

            ToTable("Salon");

            Property(salon => salon.SalonId).HasColumnName("SalonId");
            Property(salon => salon.SalonName).HasColumnName("SalonName");
            Property(salon => salon.OwnerName).HasColumnName("OwnerName");
            Property(salon => salon.SalonAdress).HasColumnName("SalonAdress");
        }
    }
}
