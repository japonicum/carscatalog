﻿using System.Data.Entity.ModelConfiguration;
using NLayerApp.DLA.Entities;

namespace NLayerApp.DLA.Context.Configurations
{
    public class CountryConfiguration : EntityTypeConfiguration<Country>
    {
        public CountryConfiguration()
        {
            //Primary Key
            HasKey(p => p.CountryId);

            //Table Name
            ToTable("Country");

            //Field Name
            Property(p => p.CountryId).HasColumnName("CountryId");
            Property(p => p.CountryName).HasColumnName("CountryName");
        }
    }
}
