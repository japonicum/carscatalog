﻿using System.Data.Entity.ModelConfiguration;
using NLayerApp.DLA.Entities;

namespace NLayerApp.DLA.Context.Configurations
{
    public class ModelConfiguration : EntityTypeConfiguration<Model>
    {
        public ModelConfiguration()
        {
            //Primary Key
            HasKey(p => p.ModelId);

            //Table Name

            ToTable("Model");

            Property(p => p.ModelId).HasColumnName("ModelId");
            Property(p => p.Color).HasColumnName("Color");
            Property(p => p.ManufactureDate).HasColumnName("DateOfManufacture").HasColumnType("date");
            Property(p => p.ModelName).HasColumnName("ModelName");
            Property(p => p.Weight).HasColumnName("Weight");
            Property(p => p.Price).HasColumnName("Price");

            //Relations FK will NULL

            HasOptional(t => t.Manufacture).
                WithMany(p=>p.Models).
                WillCascadeOnDelete(false);

            HasOptional(p => p.Salon).
                WithMany(p => p.Models).
                WillCascadeOnDelete(false);

            HasOptional(p => p.Mark).
                WithMany(p => p.Models).
                WillCascadeOnDelete(false);

        }
    }
}
