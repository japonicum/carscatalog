﻿using System.Data.Entity.ModelConfiguration;
using NLayerApp.DLA.Entities;

namespace NLayerApp.DLA.Context.Configurations
{
    public class MarkConfiguration: EntityTypeConfiguration<Mark>
    {
        public MarkConfiguration()
        {
            // Primary Key
            HasKey(p => p.MarkId);

            // Table Name
            ToTable("Mark");

            // Field Name
            Property(p => p.MarkId).HasColumnName("MarkId");
            Property(p => p.MarkName).HasColumnName("MarkName");
        }
    }
}
