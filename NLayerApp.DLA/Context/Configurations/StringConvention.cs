﻿using System.Data.Entity.ModelConfiguration.Conventions;

namespace NLayerApp.DLA.Context.Configurations
{
    class StringConvention : Convention
    {
        public StringConvention()
        {
            Properties<string>().Configure(s => s.HasMaxLength(100));
        }
    }
}
