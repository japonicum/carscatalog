﻿using System.Data.Entity;
using NLayerApp.DLA.Context.Configurations;
using NLayerApp.DLA.Entities;

namespace NLayerApp.DLA.Context
{
    public class CarContext : DbContext
    {
        public CarContext() : base("CarContext")
        {

        }

        public IDbSet<User> Users { get; set; }

        public IDbSet<Salon> Salons { get; set; }

        public IDbSet<Mark> Marks { get; set; }

        public IDbSet<Model> Models { get; set; }

        public IDbSet<Manufacture> Manufactures { get; set; }

        public IDbSet<Country> Countries { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Add Configuration mark classes
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new SalonConfiguration());
            modelBuilder.Configurations.Add(new CountryConfiguration());
            modelBuilder.Configurations.Add(new ManufactureConfiguration());
            modelBuilder.Configurations.Add(new ModelConfiguration());
            modelBuilder.Configurations.Add(new MarkConfiguration());

            //string configuration
            modelBuilder.Conventions.Add(new StringConvention());

            base.OnModelCreating(modelBuilder);
        }
    }
}
