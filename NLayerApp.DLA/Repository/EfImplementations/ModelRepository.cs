﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using NLayerApp.DLA.CustomExceptions;
using NLayerApp.DLA.Entities;
using NLayerApp.DLA.Repository.Contracts;

namespace NLayerApp.DLA.Repository.EfImplementations
{
    public class ModelRepository : Repository<Model>, IModelRepository
    {
        public ModelRepository(DbContext context) : base(context)
        {
        }
       
        public void RemoveModel(int id)
        {
            Model model = GetModelById(id);
            if (model != null)  
                Remove(model);
            else
                throw new CustomObjectNotFoundException($"Not found id: { id }");
        }

        public Model GetModelById(int id)
        {
            return DbSet.Include(p => p.Manufacture.Country).FirstOrDefault(p => p.ModelId == id);
        }
        

        public Model GetByName(string markName, string modelName, int price, string color)
        {
            
            var model = DbSet.Single(p => (p.ModelName == modelName)  &&
            (p.Price == price) && (p.Mark.MarkName == markName));

            var k = model;

            if (model != null)
                return model;

            throw new NullReferenceException(
                "Exist mark with parameters: ModelName, Price, Color - not found");
        }

        public IList<string> GetAllModelsNameCollectionString()
        {
            return DbSet.Select(p => p.ModelName).Distinct().ToList();
        }

        public List<int> GetAllPrices()
        {
            return DbSet.Select(p => p.Price).ToList();
        }
    }
}
