﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using NLayerApp.DLA.Entities;
using NLayerApp.DLA.Repository.Contracts;

namespace NLayerApp.DLA.Repository.EfImplementations
{
    public class MarkRepository : Repository<Mark>, IMarkRepository
    {
        public MarkRepository(DbContext context) : base(context)
        {

        }

        public IList<string> GetAllMarksName()
        {
                var d = DbSet.Select(p => p.MarkName).Distinct().ToList();
            return d;
        }

        public Mark GetMarkByName(string name)
        {
            return DbSet.FirstOrDefault(p => p.MarkName == name);
        }
    }
}
