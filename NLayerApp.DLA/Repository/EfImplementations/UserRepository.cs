﻿using System.Data.Entity;
using NLayerApp.DLA.Entities;
using NLayerApp.DLA.Repository.Contracts;

namespace NLayerApp.DLA.Repository.EfImplementations
{
    public class UserRepository: Repository<User>,  IUserRepository
    {
        public UserRepository(DbContext context): base(context)
        {
            
        }
    }
}
