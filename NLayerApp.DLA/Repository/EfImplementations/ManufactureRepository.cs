﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using NLayerApp.DLA.Entities;
using NLayerApp.DLA.Repository.Contracts;

namespace NLayerApp.DLA.Repository.EfImplementations
{
    public class ManufactureRepository : Repository<Manufacture>, IManufactureRepository
    {
        public ManufactureRepository(DbContext context): base(context)
        {
        }

        public IList<string> GetAllManufactureName()
        {
            return DbSet.Select(manufacture => manufacture.ManufactureName).Distinct().ToList();
        }

        public Manufacture GetManufactureByName(string name)
        {
            return DbSet.Include(o=>o.Country).FirstOrDefault(p => p.ManufactureName == name);
        }
    }
}
