﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using NLayerApp.DLA.Entities;
using NLayerApp.DLA.Repository.Contracts;

namespace NLayerApp.DLA.Repository.EfImplementations
{
    public class CountryRepository : Repository<Country>, ICountryRepository
    {
        public CountryRepository(DbContext context) : base(context)
        {

        }

        public async Task<Country> GetByNameAsync(string name)
        {
            return await DbSet.FirstOrDefaultAsync(t => t.CountryName == name);
        }

        public Country GetByName(string name)
        {
            return DbSet.FirstOrDefault(t => t.CountryName == name);
        }
    }
}
