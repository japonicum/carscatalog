﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using NLayerApp.DLA.CustomExceptions;
using NLayerApp.DLA.Entities;
using NLayerApp.DLA.Repository.Contracts;

namespace NLayerApp.DLA.Repository.EfImplementations
{
    public class SalonRepository : Repository<Salon>, ISalonRepository
    {
        public SalonRepository(DbContext context) : base(context)
        {

        }

        public Salon GetAllBindEntityWithSalon(int id)
        {
            var salon = DbSet.FirstOrDefault(p => p.SalonId == id);

            if (salon == null)
            {
                throw new CustomObjectNotFoundException($"Not found id: {id}");
            }

            return salon;
        }

        public void DeleteSalon(int id)
        {
            var salon = GetById(id);

            if (salon == null)
            {
                throw new CustomObjectNotFoundException($"Not found id: {id}");
            }
            Remove(salon);
        }

        public Salon GetBindEntitiesModelAndMark(int id)
        {
            return DbSet.Include(p => p.Models.Select(k=>k.Mark)).FirstOrDefault(o => o.SalonId == id);
        }
        
        public Salon GetSalonByName(string name)
        {
            return DbSet.Include(p => p.Models.Select(k => k.Mark)).FirstOrDefault(o => o.SalonName == name);
        }

        public IList<string> GetAllSalonName()
        {
            return DbSet.Select(p => p.SalonName).Distinct().ToList();
        }
    }
}
