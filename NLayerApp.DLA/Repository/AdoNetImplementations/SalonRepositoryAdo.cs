﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using NLayerApp.DLA.Entities;
using NLayerApp.DLA.Repository.Contracts;

namespace NLayerApp.DLA.Repository.AdoNetImplementations
{
    public class SalonRepositoryAdo : ISalonRepository
    {
        private readonly string _conectionString = ConfigurationManager.
            ConnectionStrings["AdoNetConnection"].ConnectionString;

        readonly SqlConnection _sqlConnection;

        public SalonRepositoryAdo()
        {
            _sqlConnection = new SqlConnection(_conectionString);
        }


        public Salon GetAllBindEntityWithSalon(int id)
        {
            throw new NotImplementedException();
        }

        public void DeleteSalon(int id)
        {
            throw new NotImplementedException();
        }

        public Salon GetBindEntitiesModelAndMark(int id)
        {
            throw new NotImplementedException();
        }

        public Salon GetSalonByName(string name)
        {
            throw new NotImplementedException();
        }

        public void HH(Salon salon)
        {
            throw new NotImplementedException();
        }

        public Salon GetByName(string name)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Salon> GetAllWithMarks()
        {
            _sqlConnection.Open();
            List<Salon> collectionSalonDto = new List<Salon>();

            using (var command = _sqlConnection.CreateCommand())
            {
                command.CommandText = @"SELECT * FROM Salon";
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        collectionSalonDto.Add(new Salon()
                        {
                            SalonName = reader["SalonName"].ToString(),
                            OwnerName = reader["OwnerName"].ToString(),
                            SalonAdress = reader["SalonAdress"].ToString()
                        });
                    }
                }
            }
            return collectionSalonDto.AsQueryable();
        }

        public IList<string> GetAllSalonName()
        {
            throw new NotImplementedException();
        }


        public void Create(Salon entity)
        {
            throw new NotImplementedException();
        }

        public void Remove(Salon entity)
        {
            throw new NotImplementedException();
        }

        public void Update(Salon entity)
        {
            throw new NotImplementedException();
        }

        public Salon GetById(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Salon> GetAll()
        {
            throw new NotImplementedException();
        }
    }
}
