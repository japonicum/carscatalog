﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using NLayerApp.DLA.Entities;
using NLayerApp.DLA.Repository.Contracts;

namespace NLayerApp.DLA.Repository.DapperRepository
{
    public class SalonRepositoryDapper : ISalonRepository
    {
        readonly string _conectionString = ConfigurationManager.
            ConnectionStrings["CarContext"].ConnectionString;


        public Salon GetSalonByName(string name)
        {
            throw new NotImplementedException();
        }

        public void HH(Salon salon)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Salon> GetAllWithMarks()
        {
            List<Salon> colectionSalonDto;
            using (IDbConnection db = new SqlConnection(_conectionString))
            {
                colectionSalonDto = db.Query<Salon>("SELECT * FROM Salon").ToList();
            }
            return colectionSalonDto;
        }

        public IList<string> GetAllSalonName()
        {
            throw new NotImplementedException();
        }

        public Salon GetAllBindEntityWithSalon(int id)
        {
            throw new NotImplementedException();
            
        }

        public void Create(Salon entity)
        {
            Salon salon = null;
            string sql = "INSERT INTO Salon (SalonName, OwnerName, SalonAdress) VALUES (@SalonName, @OwnerName, @SalonAdress);";
            using (var db = new SqlConnection(_conectionString))
            {
                db.Open();
                db.Execute(sql, entity);
            }
        }

        public void Remove(Salon entity)
        {
            throw new NotImplementedException();
        }

        public void Update(Salon entity)
        {
            throw new NotImplementedException();
        }

        public Salon GetById(int id)
        {
            Salon salon = null;
            string sql = "SELECT * FROM Salon WHERE SalonId = @SalonId;";
            using (var db = new SqlConnection(_conectionString))
            {
                db.Open();
                salon = db.QueryFirstOrDefault<Salon>(sql, new { SalonId = id });
            }
            return salon;
        }

        public IQueryable<Salon> GetAll()
        {
            throw new NotImplementedException();
        }



        public void DeleteSalon(int id)
        {
            throw new NotImplementedException();
        }

        public Salon GetBindEntitiesModelAndMark(int id)
        {
            throw new NotImplementedException();
        }

        public Salon GetByName(string name)
        {
            throw new NotImplementedException();
        }
    }
}
