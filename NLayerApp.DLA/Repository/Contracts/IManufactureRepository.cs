﻿using System.Collections.Generic;
using NLayerApp.DLA.Entities;

namespace NLayerApp.DLA.Repository.Contracts
{
    public interface IManufactureRepository : IRepository<Manufacture>
    {
        IList<string> GetAllManufactureName();

        Manufacture GetManufactureByName(string name);
    }
}
