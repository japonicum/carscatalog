﻿using System.Collections.Generic;
using NLayerApp.DLA.Entities;

namespace NLayerApp.DLA.Repository.Contracts
{
    public interface ISalonRepository : IRepository<Salon>
    {
        Salon GetAllBindEntityWithSalon(int id);
        
        void DeleteSalon(int id);

        Salon GetBindEntitiesModelAndMark(int id);

        Salon GetSalonByName(string name);

        IList<string> GetAllSalonName();
    }
}
