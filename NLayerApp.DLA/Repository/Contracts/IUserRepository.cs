﻿using NLayerApp.DLA.Entities;

namespace NLayerApp.DLA.Repository.Contracts
{
    public interface IUserRepository: IRepository<User>
    {

    }
}
