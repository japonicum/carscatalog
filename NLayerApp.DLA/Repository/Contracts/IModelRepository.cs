﻿using System.Collections.Generic;
using NLayerApp.DLA.Entities;

namespace NLayerApp.DLA.Repository.Contracts
{
    public interface IModelRepository : IRepository<Model>
    {
        void RemoveModel(int id);
 
        Model GetModelById(int id);

        Model GetByName(string markName, string modelName, int price, string color);
        
        IList<string> GetAllModelsNameCollectionString();

        List<int> GetAllPrices();
    }
}
