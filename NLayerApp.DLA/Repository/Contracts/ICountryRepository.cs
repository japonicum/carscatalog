﻿using System.Threading.Tasks;
using NLayerApp.DLA.Entities;

namespace NLayerApp.DLA.Repository.Contracts
{
    public interface ICountryRepository : IRepository<Country>
    {
        Task<Country> GetByNameAsync(string name);

        Country GetByName(string name);
    }
}