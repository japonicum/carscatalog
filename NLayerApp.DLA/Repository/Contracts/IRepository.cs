﻿using System.Linq;

namespace NLayerApp.DLA.Repository.Contracts
{
    public interface IRepository<TEntity> where TEntity : class
    {
        void Create(TEntity entity);

        void Remove(TEntity entity);

        void Update(TEntity entity);

        TEntity GetById(int id);

        IQueryable <TEntity> GetAll();
    }
}
