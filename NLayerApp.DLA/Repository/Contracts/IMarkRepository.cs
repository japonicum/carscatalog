﻿using System.Collections.Generic;
using NLayerApp.DLA.Entities;

namespace NLayerApp.DLA.Repository.Contracts
{
    public interface IMarkRepository : IRepository<Mark>
    {
        IList<string> GetAllMarksName();

        Mark GetMarkByName(string name);
    }
}
