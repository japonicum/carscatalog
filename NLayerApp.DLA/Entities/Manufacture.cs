﻿using System.Collections.Generic;

namespace NLayerApp.DLA.Entities
{
    public class Manufacture
    {
        public Manufacture()
        {
            Models = new List<Model>();
        }
        public int ManufactureId { get; set; }

        public string ManufactureName { get; set; }

        public  Country Country { get; set; }

        public ICollection<Model> Models { get; set; }
    }
}
