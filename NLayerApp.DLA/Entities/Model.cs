﻿using System;
using System.Collections.Generic;
using Common.Enums;

namespace NLayerApp.DLA.Entities
{
    public class Model
    {
        public int ModelId { get; set; }

        public string ModelName { get; set; }

        public int Price { get; set; }

        public DateTime ManufactureDate { get; set; }

        public CarColors Color { get; set; }

        public float Weight { get; set; }

        public Manufacture Manufacture { get; set; }

        public Mark  Mark { get; set; }

        public Salon Salon { get; set; }
    }
}
