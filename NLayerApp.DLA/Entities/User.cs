﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NLayerApp.DLA.Entities
{
    public class User
    {
        public int  UserId { get; set; }

        public string UserName { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
