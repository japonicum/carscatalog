﻿using System.Collections.Generic;

namespace NLayerApp.DLA.Entities
{
    public class Country
    {
        public Country()
        {
            Manufactures = new List<Manufacture>();
        }

        public int CountryId { get; set; }

        public string CountryName { get; set; }

        public ICollection<Manufacture> Manufactures  { get; set; }
    }
}
