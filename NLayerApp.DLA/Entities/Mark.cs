﻿using System.Collections.Generic;

namespace NLayerApp.DLA.Entities
{
    public class Mark
    {
        public Mark()
        {
            Models = new List<Model>();
        }

        public int MarkId { get; set; }

        public string MarkName { get; set; }

        public ICollection<Model> Models { get; set; }
    }
}
