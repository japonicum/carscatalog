﻿
using System.Collections.Generic;

namespace NLayerApp.DLA.Entities
{
    public class Salon
    {
        public Salon()
        {
            Models = new List<Model>();
        }

        public int SalonId { get; set; }

        public string SalonName { get; set; }

        public string OwnerName { get; set; }

        public string SalonAdress { get; set; }

        public ICollection<Model> Models { get; set; }
    }
}
