namespace NLayerApp.DLA.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editDbRilations : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Mark", "Salon_SalonId", "dbo.Salon");
            DropIndex("dbo.Mark", new[] { "Salon_SalonId" });
            AddColumn("dbo.Salon", "Mark_MarkId", c => c.Int());
            CreateIndex("dbo.Salon", "Mark_MarkId");
            AddForeignKey("dbo.Salon", "Mark_MarkId", "dbo.Mark", "MarkId");
            DropColumn("dbo.Mark", "Salon_SalonId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Mark", "Salon_SalonId", c => c.Int());
            DropForeignKey("dbo.Salon", "Mark_MarkId", "dbo.Mark");
            DropIndex("dbo.Salon", new[] { "Mark_MarkId" });
            DropColumn("dbo.Salon", "Mark_MarkId");
            CreateIndex("dbo.Mark", "Salon_SalonId");
            AddForeignKey("dbo.Mark", "Salon_SalonId", "dbo.Salon", "SalonId");
        }
    }
}
