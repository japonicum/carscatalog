namespace NLayerApp.DLA.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editDB : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Model", "Salon_SalonId", "dbo.Salon");
            DropIndex("dbo.Model", new[] { "Salon_SalonId" });
            AddColumn("dbo.Mark", "Salon_SalonId", c => c.Int());
            CreateIndex("dbo.Mark", "Salon_SalonId");
            AddForeignKey("dbo.Mark", "Salon_SalonId", "dbo.Salon", "SalonId");
            DropColumn("dbo.Model", "Salon_SalonId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Model", "Salon_SalonId", c => c.Int());
            DropForeignKey("dbo.Mark", "Salon_SalonId", "dbo.Salon");
            DropIndex("dbo.Mark", new[] { "Salon_SalonId" });
            DropColumn("dbo.Mark", "Salon_SalonId");
            CreateIndex("dbo.Model", "Salon_SalonId");
            AddForeignKey("dbo.Model", "Salon_SalonId", "dbo.Salon", "SalonId");
        }
    }
}
