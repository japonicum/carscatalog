using Common.Enums;
using NLayerApp.DLA.Entities;

namespace NLayerApp.DLA.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public sealed class Configuration : DbMigrationsConfiguration<Context.CarContext>
    {
        public Configuration()
        {

        }

        protected override void Seed(Context.CarContext context)
        {
            Country oneCountry = new Country() { CountryId = 1, CountryName = "JAP" };
            Country twoCountry = new Country() { CountryId = 2, CountryName = "USA" };

            context.Countries.AddOrUpdate(p => p.CountryId, oneCountry);
            context.Countries.AddOrUpdate(p => p.CountryId, twoCountry);


            Manufacture oneManufacture = new Manufacture { ManufactureId = 1, ManufactureName = "London", Country = oneCountry };
            Manufacture twoManufacture = new Manufacture { ManufactureId = 2, ManufactureName = "Berlin", Country = twoCountry };

            context.Manufactures.AddOrUpdate(p => p.ManufactureId, twoManufacture);
            context.Manufactures.AddOrUpdate(p => p.ManufactureId, oneManufacture);



            Salon oneSalon = new Salon()
            {
                SalonId = 1,
                OwnerName = "Jim",
                SalonAdress = "Lake str, 125",
                SalonName = "HondaInter",
            };

            Salon twoSalon = new Salon()
            {
                SalonId = 2,
                OwnerName = "Wiliam",
                SalonAdress = "Korlina str, 128, Dalass",
                SalonName = "VolvoInter",
            };

            context.Salons.AddOrUpdate(p => p.SalonId, oneSalon);
            context.Salons.AddOrUpdate(p => p.SalonId, twoSalon);


            Mark oneMark = new Mark { MarkId = 1, MarkName = "Honda" };
            Mark twoMark = new Mark { MarkId = 2, MarkName = "Volvo" };

            context.Marks.AddOrUpdate(p => p.MarkId, oneMark);
            context.Marks.AddOrUpdate(p => p.MarkId, twoMark);

            Model oneModel = new Model
            {
                ModelId = 1,
                Color = CarColors.Blue,
                ManufactureDate = DateTime.MinValue,
                Price = 20000,
                Weight = 2f,
                ModelName = "Civic",
                Manufacture = oneManufacture,
                Mark = oneMark,
                Salon = oneSalon
            };

            Model twoModel = new Model
            {
                ModelId = 2,
                Color = CarColors.Blue,
                ManufactureDate = DateTime.MinValue,
                Price = 56000,
                Weight = 1.6f,
                ModelName = "CRV",
                Manufacture = twoManufacture,
                Mark = oneMark,
                Salon = oneSalon
            };

            Model threeModel = new Model
            {
                ModelId = 3,
                Color = CarColors.Blue,
                ManufactureDate = DateTime.MinValue,
                Price = 51000,
                Weight = 1.4f,
                ModelName = "V40",
                Manufacture = twoManufacture,
                Mark = twoMark,
                Salon = twoSalon
            };

            context.Models.AddOrUpdate(p => p.ModelId, oneModel);
            context.Models.AddOrUpdate(p => p.ModelId, twoModel);
            context.Models.AddOrUpdate(p => p.ModelId, threeModel);





            context.SaveChanges();
        }
    }
}

