namespace NLayerApp.DLA.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Country",
                c => new
                    {
                        CountryId = c.Int(nullable: false, identity: true),
                        CountryName = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.CountryId);
            
            CreateTable(
                "dbo.Manufacture",
                c => new
                    {
                        ManufactureId = c.Int(nullable: false, identity: true),
                        ManufactureName = c.String(maxLength: 100),
                        Country_CountryId = c.Int(),
                    })
                .PrimaryKey(t => t.ManufactureId)
                .ForeignKey("dbo.Country", t => t.Country_CountryId)
                .Index(t => t.Country_CountryId);
            
            CreateTable(
                "dbo.Model",
                c => new
                    {
                        ModelId = c.Int(nullable: false, identity: true),
                        ModelName = c.String(maxLength: 100),
                        Price = c.Int(nullable: false),
                        DateOfManufacture = c.DateTime(nullable: false, storeType: "date"),
                        Color = c.Int(nullable: false),
                        Weight = c.Single(nullable: false),
                        Manufacture_ManufactureId = c.Int(),
                        Mark_MarkId = c.Int(),
                        Salon_SalonId = c.Int(),
                    })
                .PrimaryKey(t => t.ModelId)
                .ForeignKey("dbo.Manufacture", t => t.Manufacture_ManufactureId)
                .ForeignKey("dbo.Mark", t => t.Mark_MarkId)
                .ForeignKey("dbo.Salon", t => t.Salon_SalonId)
                .Index(t => t.Manufacture_ManufactureId)
                .Index(t => t.Mark_MarkId)
                .Index(t => t.Salon_SalonId);
            
            CreateTable(
                "dbo.Mark",
                c => new
                    {
                        MarkId = c.Int(nullable: false, identity: true),
                        MarkName = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.MarkId);
            
            CreateTable(
                "dbo.Salon",
                c => new
                    {
                        SalonId = c.Int(nullable: false, identity: true),
                        SalonName = c.String(maxLength: 100),
                        OwnerName = c.String(maxLength: 100),
                        SalonAdress = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.SalonId);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(maxLength: 100),
                        Password = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Model", "Salon_SalonId", "dbo.Salon");
            DropForeignKey("dbo.Model", "Mark_MarkId", "dbo.Mark");
            DropForeignKey("dbo.Model", "Manufacture_ManufactureId", "dbo.Manufacture");
            DropForeignKey("dbo.Manufacture", "Country_CountryId", "dbo.Country");
            DropIndex("dbo.Model", new[] { "Salon_SalonId" });
            DropIndex("dbo.Model", new[] { "Mark_MarkId" });
            DropIndex("dbo.Model", new[] { "Manufacture_ManufactureId" });
            DropIndex("dbo.Manufacture", new[] { "Country_CountryId" });
            DropTable("dbo.User");
            DropTable("dbo.Salon");
            DropTable("dbo.Mark");
            DropTable("dbo.Model");
            DropTable("dbo.Manufacture");
            DropTable("dbo.Country");
        }
    }
}
