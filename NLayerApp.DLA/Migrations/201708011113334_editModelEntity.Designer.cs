// <auto-generated />
namespace NLayerApp.DLA.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class editModelEntity : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(editModelEntity));
        
        string IMigrationMetadata.Id
        {
            get { return "201708011113334_editModelEntity"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
