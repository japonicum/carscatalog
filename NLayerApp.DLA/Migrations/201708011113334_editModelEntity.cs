namespace NLayerApp.DLA.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editModelEntity : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Salon", "Mark_MarkId", "dbo.Mark");
            DropIndex("dbo.Salon", new[] { "Mark_MarkId" });
            AddColumn("dbo.Model", "Salon_SalonId", c => c.Int());
            CreateIndex("dbo.Model", "Salon_SalonId");
            AddForeignKey("dbo.Model", "Salon_SalonId", "dbo.Salon", "SalonId");
            DropColumn("dbo.Salon", "Mark_MarkId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Salon", "Mark_MarkId", c => c.Int());
            DropForeignKey("dbo.Model", "Salon_SalonId", "dbo.Salon");
            DropIndex("dbo.Model", new[] { "Salon_SalonId" });
            DropColumn("dbo.Model", "Salon_SalonId");
            CreateIndex("dbo.Salon", "Mark_MarkId");
            AddForeignKey("dbo.Salon", "Mark_MarkId", "dbo.Mark", "MarkId");
        }
    }
}
