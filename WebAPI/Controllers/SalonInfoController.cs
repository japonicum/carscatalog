﻿using System.Web.Mvc;

namespace WebAPI.Controllers
{
    public class SalonInfoController : Controller
    {
        public ActionResult Index(int id)
        {
            TempData["data"] = id;
            return View();
        }
    }
}