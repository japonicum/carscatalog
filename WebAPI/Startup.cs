﻿using System;
using System.Data.Entity;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Configuration;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using NLayerApp.DLA.Context;
using Owin;
using WebAPI.Provider;

[assembly: OwinStartup(typeof(WebAPI.Startup))]

namespace WebAPI
{
    public class Startup
    {
        public static void Configuration(IAppBuilder app)
        {
            var oAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/token"),
                Provider = new ApplicationOAuthProvider(),
                AccessTokenExpireTimeSpan = TimeSpan.FromHours(3),

                AllowInsecureHttp = true
            };
            
            RegisterGlobalAsaxWebApi();
            RegisterAutoMigration();
            app.UseOAuthBearerTokens(oAuthOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

            AutofacConfig.Configure(typeof(Startup).Assembly);
        }

        public static void RegisterGlobalAsaxWebApi()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        public static void RegisterAutoMigration()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<CarContext, NLayerApp.DLA.Migrations.Configuration>());
        }
    }
}
