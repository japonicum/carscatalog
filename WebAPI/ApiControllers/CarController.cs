﻿using System.Web.Http;
using NLayerApp.BL.DTO;
using NLayerApp.BL.IServices;

namespace WebAPI
{
    [Authorize]
    public class CarController : ApiController
    {
        private readonly IModelServices _modelServices;

        public CarController(IModelServices modelServices)
        {
            _modelServices = modelServices;
        }

        [HttpGet]
        public IHttpActionResult GetAll()
        {
            return Ok(_modelServices.GetAllCarsDto());
        }


        [HttpPost]
        public void PostCreateCar(CarsFullInformationDto carDto)
        {
            _modelServices.AddCarDto(carDto);
        }

        [HttpPut]
        public void PutUpdateCar(CarsFullInformationDto carDto)
        {
            _modelServices.UpdateCarInformationDto(carDto);
        }

        [HttpDelete]
        public IHttpActionResult DeleteCar(int id)
        {
            return Ok(_modelServices.DeleteCarDto(id));
        }
    }
}
