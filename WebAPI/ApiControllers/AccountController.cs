﻿
using System.Web.Http;
using NLayerApp.BL.DTO;
using NLayerApp.BL.IServices;

namespace WebAPI
{
    [AllowAnonymous]
    public class AccountController : ApiController
    {
        private readonly IUserServices _userServices;

        public AccountController(IUserServices userServices)
        {
            _userServices = userServices;
        }
    
        [HttpPost]
        public void Post(UserAuthorizetionDto userAuthorizetion)
        {
            _userServices.AddUser(userAuthorizetion);
        }
    }
}
