﻿using System.Web.Http;
using NLayerApp.BL.DTO.SalonDtos;
using NLayerApp.BL.IServices;

namespace WebAPI
{
    [Authorize]
    public class SalonFullInfoController : ApiController
    {
        private readonly ISalonInfoServices _salonInfoServices;

        public SalonFullInfoController(ISalonInfoServices salonInfoServices)
        {
            _salonInfoServices = salonInfoServices;
        }

        [HttpGet]
        [Route("api/salonfullinfo/{salonId:int}/")]
        public IHttpActionResult Get(int salonId)
        {
            return Ok(_salonInfoServices.GetSalonInfoCollectionDto(salonId));
        }

        [HttpPost]
        public SalonInfoCollectionDto PostSortModel(SortModelDto sortModelDto)
        {
            return _salonInfoServices.SearchAvailbelData(sortModelDto);
        }

        [HttpPost]
        [Route("api/salonfullinfo/addMark/")]
        public void AddModelToSalon(SalonInfoDto salonInfoDto)
        {
            _salonInfoServices.AddCarToSalon(salonInfoDto);
        }
    }
}
