﻿using System.Web.Http;
using NLayerApp.BL.DTO.SalonDtos;
using NLayerApp.BL.IServices;
using NLayerApp.DLA.Repository.Contracts;

namespace WebAPI
{
    [Authorize]
    public class SalonsController : ApiController
    {
        private readonly ISalonServices _salonServices;
        
        public SalonsController(ISalonServices salonServices)
        {
            _salonServices = salonServices;
        }

        [HttpGet]
        public IHttpActionResult Get()
        {
            return Ok(_salonServices.GetAllSalonsDto());
        }

        [HttpGet]
        public IHttpActionResult GetById(int id)
        {
            return Ok(_salonServices.GetSalonByIdDto(id));
        }

        [HttpPut]
        public IHttpActionResult Update(SalonDto salonDto)
        {
            return Ok(_salonServices.UpdateSalonDto(salonDto));
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            return Ok(_salonServices.DeleteSalonDto(id));
        }

        [HttpPost]
        public IHttpActionResult Add(SalonDto salonDto)
        {
            return Ok(_salonServices.AddSalonDto(salonDto));
        }
    }
}
