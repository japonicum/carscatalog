﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Common.Enums;
using Moq;
using NLayerApp.BL.DTO;
using NLayerApp.BL.Services;
using NLayerApp.BL.Services.ServiceResults;
using NLayerApp.DLA.Context;
using NLayerApp.DLA.Entities;
using NLayerApp.DLA.Repository.Contracts;
using NLayerApp.DLA.UnitOfWork;
using NUnit.Framework;

namespace WebAppAndersen.Tests
{

    [TestFixture]
    public class NUnitTest
    {
        // DbContext
        private Mock<CarContext> _contextMock;


        // Services
        private ModelServices _modelServices;

        // UnitOfWork
        private UnitOfWork _uow;
        private Mock<IUnitOfWork> _mockUow;

        // DbSet<TEntity>
        private Mock<DbSet<Mark>> _mockMarkRepo;
        private Mock<DbSet<Model>> _mockModelRepo;
        private Mock<DbSet<Manufacture>> _mockManufactureRepo;
        private Mock<DbSet<Country>> _mockCountryRepo;
        private Mock<DbSet<Salon>> _mockSalonRepo;

        private Mock<IModelRepository> _mockIModelRepository;
        private Mock<IMarkRepository> _mockIMarkRepository;
        private Mock<IManufactureRepository> _mockIManufactureRepository;
        private Mock<ICountryRepository> _mockICountryRepository;
        private Mock<ISalonRepository> _mockISalonRepository;

        // DTO
        private CarsFullInformationDto _carDto;

        // Entitys
        private List<Salon> _salons;
        private List<Mark> _marks;
        private List<Model> _models;
        private List<Manufacture> _manufactures;
        private List<Country> _countries;

        private int _id;
        private int _index;
        private string _countryName;


        [SetUp]
        public void SetUpTest()
        {
            _id = 1;
            _index = 0;
            _countryName = "UK";

            _contextMock = new Mock<CarContext>();

            _mockSalonRepo = new Mock<DbSet<Salon>>();
            _mockModelRepo = new Mock<DbSet<Model>>();
            _mockMarkRepo = new Mock<DbSet<Mark>>();
            _mockManufactureRepo = new Mock<DbSet<Manufacture>>();
            _mockCountryRepo = new Mock<DbSet<Country>>();

            _mockISalonRepository = new Mock<ISalonRepository>();
            _mockIModelRepository = new Mock<IModelRepository>();
            _mockIMarkRepository = new Mock<IMarkRepository>();
            _mockIManufactureRepository = new Mock<IManufactureRepository>();
            _mockICountryRepository = new Mock<ICountryRepository>();

           // _uow = new UnitOfWork(_contextMock.Object);
            _mockUow = new Mock<IUnitOfWork>();
            _modelServices = new ModelServices(_mockUow.Object);

            _carDto = new CarsFullInformationDto
            {
                Color = "Blue",
                ModelName = "Civic",
                MarkName = "Honda",
                Weight = 44f,
                Price = 111000,
                Id = 1,
                ManufactureName = "Lviv"
            };

            _countries = new List<Country>
            {
                new Country
                {
                    CountryId = 1,
                    CountryName = "UK",
                    Manufactures = new List<Manufacture>()
                },
                new Country
                {
                    CountryId = 2,
                    CountryName = "US",
                    Manufactures = new List<Manufacture>()
                }
            };

            _manufactures = new List<Manufacture>()
            {
                new Manufacture
                {
                    ManufactureId = 1,
                    ManufactureName = "Lviv",
                    Country = _countries[0],
                },
                new Manufacture
                {
                    ManufactureId = 2,
                    ManufactureName = "Lemberg",
                    Country = _countries[1],
                }
            };



            _marks = new List<Mark>
            {
                new Mark
                {
                    MarkId = 1,
                    MarkName = "Honda"
                },
                new Mark {
                    MarkId = 2,
                    MarkName = "WV"
                }
            };

            _salons = new List<Salon>
            {
                new Salon {
                    SalonId = 1,
                    OwnerName = "OWWWW",
                    SalonAdress = "ADD",
                    SalonName = "NAA"
                },

                new Salon {
                    SalonId = 2,
                    OwnerName = "KKOK",
                    SalonAdress = "IIII",
                    SalonName = "YYY"
                }
            };

            _models = new List<Model>()
            {
                new Model {
                    ModelId = 1,
                    Manufacture = _manufactures[0],
                    Color = CarColors.Blue,
                    ModelName = "Seed",
                    Mark = _marks[0],
                    Price = 56000,
                    Weight = 1.2f,
                    Salon = _salons[0],
                    ManufactureDate = DateTime.Now
                },

                new Model {
                    ModelId = 2,
                    Manufacture = _manufactures[1],
                    Color = CarColors.Blue,
                    ModelName = "Civic",
                    Mark = _marks[1],
                    Price = 33000,
                    Weight = 2f,
                    Salon = _salons[1],
                    ManufactureDate = DateTime.Now
                },
            };


            // Mock Collection AsIQueryable
            _mockCountryRepo.MockQueryable(_countries);
            _mockManufactureRepo.MockQueryable(_manufactures);
            _mockModelRepo.MockQueryable(_models);
            _mockMarkRepo.MockQueryable(_marks);
            _mockSalonRepo.MockQueryable(_salons);


            //// Mock Entities Repositories
            _mockUow.SetupGet(p => p.SalonRepository).Returns(_mockISalonRepository.Object);
            _mockUow.SetupGet(v => v.ModelRepository).Returns(_mockIModelRepository.Object);
            _mockUow.SetupGet(v => v.MarkRepository).Returns(_mockIMarkRepository.Object);
            _mockUow.SetupGet(l => l.ManufactureRepository).Returns(_mockIManufactureRepository.Object);
            _mockUow.SetupGet(l => l.CountryRepository).Returns(_mockICountryRepository.Object);
        }

        [Test]
        public void TestMethod_Context()
        {
            // Extention: Mock<Dbset>  
            //var model = 
            //_contextMock.Setup(p=>p.Set<Model>()).Returns()
            //_contextMock.Setup(x => x.Set<CarContext>());


            // Act


            // Assert

        }

        [Test]
        public void ModelServices_Delete_Test()
        {
            _mockIModelRepository.Setup(l => l.GetModelById(_id)).Returns(_models[_index]);
            _mockIModelRepository.Setup(k => k.RemoveModel(_id));

            var model = _modelServices.DeleteCarDto(_id);

            var sr = new ServiceResult()
            {
                Error = "Well",
                Success = true
            };

            Assert.AreEqual(model.Error, sr.Error);
            _mockIModelRepository.Verify(p => p.RemoveModel(It.Is<int>(o => o == _id)), Times.Once);
        }

        [Test]
        public void ModelServices_GetById_Test()
        {
            //Arange
            //_mockIModelRepository.Setup(h => h.GetModelAndAllBindEntitiesById(_id))
            //    .Returns(_models[_index]);

            //// Act
            //var model = _modelServices.GetByIdCarDto(_id);

            //// Assert
            //Assert.AreEqual(model.ModelName, _carDto.ModelName);
            //Assert.AreEqual(model.Color, _carDto.Color);
            //Assert.AreEqual(model.CountryName, _carDto.CountryName);
            // Assert.AreEqual(model.Price, _carDto.Price);

            //_mockIModelRepository.Verify(c => c.GetModelAndAllBindEntitiesById(It.IsAny<int>()), 
            //    Times.Once);
        }

        [Test]
        public void ModelServices_Save_Test()
        {
            Model model = new Model();

            //Arange
            _mockIModelRepository.Setup(p => p.Create(_models[0]));

            _mockIMarkRepository.Setup(p => p.GetMarkByName(_carDto.MarkName)).Returns(_marks[0]);

            _mockIManufactureRepository.Setup(p => p.GetManufactureByName(_carDto.ManufactureName))
                .Returns(_manufactures[0]);

            // Act
            var m = _modelServices.FillModelFromDto(model, _carDto);
            _modelServices.AddCarDto(_carDto);

            // Assert
            Assert.AreEqual(m.ModelName, model.ModelName);
            _mockIModelRepository.Verify(x => x.Create(It.Is<Model>(p => p.Manufacture == model.Manufacture)), Times.Once);
        }

        [Test]
        public void ModelServices_Update_Test()
        {
            _mockIModelRepository.Setup(o => o.GetModelById(_carDto.Id)).Returns(_models[_index]);
            _mockIModelRepository.Setup(o => o.Update(_models[_index]));

            _modelServices.FillModelFromDto(_models[_index], _carDto);
            _modelServices.UpdateCarInformationDto(_carDto);

            Assert.AreEqual(_models[_index].ModelName, _carDto.ModelName);
            Assert.AreEqual(_countries[_index].Manufactures, _carDto.ManufactureName);

            //_mockIModelRepository.Verify(o => o.Update(It.Is<Model>()), Times.Once);
        }

        [Test]
        public void ModelServices_GetAll_Test()
        {
            //InfoModelDto _infoModelDto = new InfoModelDto()
            //{
            //    CollectionColorsDto =
            //}

            //_mockIModelRepository.Setup(d => d.GetAll()).Returns(_models.AsQueryable());

            //InfoForDto listDto = _modelServices.GetAllCarsDto();

            //Assert.AreEqual(listDto.CollectionCarsFullInformationDto.Count(),  );

        }


    }
}
