﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Moq;  

namespace WebAppAndersen.Tests
{
    public  static class ExtentionMockIQueryable
    {
        public static void MockQueryable<T> (this Mock<DbSet<T>> mockSet, IList<T> l) where T : class
        {
            mockSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(l.AsQueryable().Provider);
            mockSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(l.AsQueryable().Expression);
            mockSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(l.AsQueryable().ElementType);
            mockSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(l.AsQueryable().GetEnumerator);
        }
        
    }

    

}
