﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NLayerApp.BL.DTO;
using NLayerApp.DLA.Entities;
using NLayerApp.DLA.Repository.Contracts;
using NUnit.Framework;

namespace WebAppAndersen.Tests.TestMarkRepo
{
    [TestFixture]
    public class MarkRepository_Test
    {
        private Mock<DbSet<Mark>> _mockMarkRepo;
        private Mock<IMarkRepository> _mockIMarkRepository;
        private List<Mark> _marks;
        private CarsFullInformationDto _carDto;

        [SetUp]
        public void SetUpMarkEntity()
        {
            _mockMarkRepo = new Mock<DbSet<Mark>>();

            _mockIMarkRepository = new Mock<IMarkRepository>();

            _carDto = new CarsFullInformationDto();

            _marks = new List<Mark>
            {
                new Mark
                {
                    MarkId = 1,
                    MarkName = "Honda"
                },
                new Mark {
                    MarkId = 2,
                    MarkName = "WV"
                }
            };

            _carDto = new CarsFullInformationDto
            {
                Id = 1,
                Color = "Blue",
                ModelName = "Civic",
                MarkName = "Honda",
                Weight = 44f,
                Price = 111000,
                ManufactureName = "Lviv"
            };
        }


        [Test]
        public void TestGetByName()
        {
            // Assert
            _mockIMarkRepository.Setup(p => p.GetMarkByName(_carDto.MarkName)).Returns(_marks[0]);

            

            // Act


        }
    }
}
