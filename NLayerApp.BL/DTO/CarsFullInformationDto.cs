﻿namespace NLayerApp.BL.DTO
{
    public class CarsFullInformationDto
    {
        public int Id { get; set; }

        public string ModelName { get; set; }

        public string MarkName { get; set; }

        public string ManufactureName { get; set; }

        public float Weight { get; set; }

        public string Color { get; set; }

        public int Price { get; set; }
    }
}
