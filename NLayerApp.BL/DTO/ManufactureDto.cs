﻿
namespace NLayerApp.BL.DTO
{
    public class ManufactureDto
    {
        public int Id { get; set; }
        public string ManufactureName { get; set; }
        public int  CountryId { get; set; }
    }
}
