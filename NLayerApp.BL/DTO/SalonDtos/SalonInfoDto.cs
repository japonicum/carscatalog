﻿

namespace NLayerApp.BL.DTO.SalonDtos
{
    public class SalonInfoDto
    {
        public int SalonInfoId { get; set; }

        public string SalonInfoSalonName { get; set; }

        public string SalonInfoModelName { get; set; }

        public string SalonInfoMarkName { get; set; }

        public int SalonInfoPrice { get; set; }

        public string SalonInfoColor { get; set; }

        public int SalonInfoTotal { get; set; }
    }
}
