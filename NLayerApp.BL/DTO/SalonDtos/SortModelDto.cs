﻿
namespace NLayerApp.BL.DTO.SalonDtos
{
    public class SortModelDto
    {
        public string ModelName { get; set; }
        public string MarkName { get; set; }
        public string SalonName { get; set; }
    }
}
