﻿
namespace NLayerApp.BL.DTO.SalonDtos
{
    public class ModelCountDto
    {
        public string ModelName { get; set; }

        public int Price { get; set; }

        public string Color { get; set; }

        public int  Total { get; set; }
    }
}
