﻿using System.Collections.Generic;

namespace NLayerApp.BL.DTO.SalonDtos
{
    public class SalonsDto
    {
        public IEnumerable<SalonDto> CollectionSalonDto { get; set; }
    }
}
