﻿namespace NLayerApp.BL.DTO.SalonDtos
{
    public class SalonDto
    {
        public int SalonId { get; set; }

        public string SalonName { get; set; }

        public string OwnerName { get; set; }

        public string Adress { get; set; }
    }
}
