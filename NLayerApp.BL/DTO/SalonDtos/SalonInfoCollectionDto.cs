﻿using System.Collections;
using System.Collections.Generic;
using Common.Enums;

namespace NLayerApp.BL.DTO.SalonDtos
{
    public class SalonInfoCollectionDto : SalonsDto
    {
        public string CurrentSalonName { get; set; }

        public IEnumerable<SalonInfoDto> CollectionSalonInfoDto { get; set; }

        public IEnumerable<string> CollectionModelName { get;set; }

        public IEnumerable CollectionMarkName { get; set; }

        public IEnumerable<string> CollectionColors { get; set; }

        public IEnumerable<int> CollectionPrices { get; set; }
    }
}
