﻿namespace NLayerApp.BL.DTO
{
    public class ModelDto
    {
        public int Id { get; set; }
        public string ModelName { get; set; }
        public string MarkName { get; set; }
        public int Price { get; set; }
    }
}
