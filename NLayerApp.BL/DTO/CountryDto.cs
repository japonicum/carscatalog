﻿
namespace NLayerApp.BL.DTO
{
    public class CountryDto
    {
        public int  Id { get; set; }
        public string CountryName { get; set; }
    }
}
