﻿using System;

namespace NLayerApp.BL.DTO
{
    public class MarkDto
    {
        public int Id { get; set; }
        public string MarkName { get; set; }
        public DateTime? DateOfProduction { get; set; }
        public string ModelName { get; set; }
    }
}
