﻿using System.Collections;
using System.Collections.Generic;

namespace NLayerApp.BL.DTO
{
    public class InfoModelDto
    {
        public IEnumerable<string> CollectionColorsDto { get; set; }

        public IEnumerable CollectionModelsDto { get; set; }

        public IEnumerable CollectionMarksDto { get; set; }

        public IEnumerable CollectionManufacturesDto { get; set; }

        public IEnumerable<CarsFullInformationDto> CollectionCarsFullInformationDto { get; set; }
    }
}