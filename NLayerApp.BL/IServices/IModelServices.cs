﻿using NLayerApp.BL.DTO;
using NLayerApp.BL.Services.ServiceResults;

namespace NLayerApp.BL.IServices
{
    public interface IModelServices 
    {
        InfoModelDto GetAllCarsDto();

        void AddCarDto(CarsFullInformationDto carDto);

        void UpdateCarInformationDto(CarsFullInformationDto carDto);

        ServiceResult DeleteCarDto(int id);
    }
}
