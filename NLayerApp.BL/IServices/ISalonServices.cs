﻿using NLayerApp.BL.DTO.SalonDtos;
using NLayerApp.BL.Services.ServiceResults;

namespace NLayerApp.BL.IServices
{
    public interface ISalonServices
    {
        SalonInfoCollectionDto GetAllSalonsDto();

        SalonDto GetSalonByIdDto(int id);

        ServiceResult UpdateSalonDto(SalonDto salon);

        ServiceResult AddSalonDto(SalonDto salonDto);

        ServiceResult DeleteSalonDto(int id);
    }
}
