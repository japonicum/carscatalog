﻿using System.Collections.Generic;
using NLayerApp.BL.DTO;

namespace NLayerApp.BL.IServices
{
    public interface ICountryServices
    {
        IList<CountryDto> GetAllCountriesDto();

        CountryDto GetCountryDtoById(int i);
    }
}
