﻿using NLayerApp.BL.DTO.SalonDtos;
using NLayerApp.BL.Services.ServiceResults;

namespace NLayerApp.BL.IServices
{
    public interface ISalonInfoServices
    {
        SalonInfoCollectionDto GetSalonInfoCollectionDto(int id);

        ServiceResult AddCarToSalon(SalonInfoDto salonInfoDto);

        SalonInfoCollectionDto SearchAvailbelData(SortModelDto sortModelDto);
    }
}
