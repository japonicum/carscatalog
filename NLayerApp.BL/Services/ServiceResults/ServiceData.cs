﻿namespace NLayerApp.BL.Services.ServiceResults
{
    public class ServiceData<TEntity> : ServiceResult
        where TEntity : class
    {
        public TEntity Data { get; set; }
    }
}
