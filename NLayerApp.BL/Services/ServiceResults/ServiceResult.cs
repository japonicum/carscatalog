﻿namespace NLayerApp.BL.Services.ServiceResults
{
    public class ServiceResult
    {
        public string Error { get; set; }
        public bool Success { get; set; }
    }
}
