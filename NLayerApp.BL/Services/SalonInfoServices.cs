﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Helpers;
using NLayerApp.BL.DTO.SalonDtos;
using NLayerApp.BL.IServices;
using NLayerApp.BL.Services.ServiceResults;
using NLayerApp.DLA.CustomExceptions;
using NLayerApp.DLA.Entities;
using NLayerApp.DLA.UnitOfWork;

namespace NLayerApp.BL.Services
{
    public class SalonInfoServices : BaseServices, ISalonInfoServices
    {
        public SalonInfoServices(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }

        public ServiceResult AddCarToSalon(SalonInfoDto salonInfoDto)
        {
            ServiceResult sr = new ServiceResult() { Error = "", Success = true };

            try
            {
                var model = GetModelFromSalonInfoDto(salonInfoDto);

                var salon = Uow.SalonRepository.GetSalonByName(salonInfoDto.SalonInfoSalonName);

                for (int i = 0; i < salonInfoDto.SalonInfoTotal; i++)
                {
                    salon.Models.Add(model);
                }
                Uow.SaveChanges();
            }
            catch (CustomObjectNotFoundException)
            {
                sr.Error = "Not Found";
                sr.Success = false;
            }
            catch (Exception)
            {
                sr.Error = "Not Found";
                sr.Success = false;
            }

            return sr;
        }

        private Model GetModelFromSalonInfoDto(SalonInfoDto salonInfoDto)
        {
            return Uow.ModelRepository.GetByName(salonInfoDto.SalonInfoMarkName,
                   salonInfoDto.SalonInfoModelName,
                   salonInfoDto.SalonInfoPrice,
                   salonInfoDto.SalonInfoColor);
        }


        public SalonInfoCollectionDto GetSalonInfoCollectionDto(int id)
        {
            var salon = Uow.SalonRepository.GetBindEntitiesModelAndMark(id);

            var ee = salon.Models.
                GroupBy(p => new { p.
                    Mark.MarkName,
                    p.ModelName,
                    p.Color,
                    p.Price })
                .Select(o => new { o.Key.MarkName,
                    o.Key.Color,
                    o.Key.Price,
                    o.Key.ModelName,
                    total = o.Count() });


            var ss = new SalonInfoCollectionDto()
            {
                CurrentSalonName = salon.SalonName,
                CollectionSalonInfoDto = ee.Select(model => new SalonInfoDto()
                {
                    SalonInfoMarkName = model.MarkName,
                    SalonInfoModelName = model.ModelName,
                    SalonInfoPrice = model.Price,
                    SalonInfoColor = EnumHelpers.GetStringColor(model.Color),
                    SalonInfoTotal = model.total
                }),

                CollectionModelName = Uow.ModelRepository.GetAllModelsNameCollectionString(),

                CollectionMarkName = Uow.MarkRepository.GetAllMarksName(),

                CollectionColors = EnumHelpers.GetColorsEnum(),

                CollectionPrices = Uow.ModelRepository.GetAllPrices()
            };

            return ss;
        }

        public SalonInfoCollectionDto SearchAvailbelData(SortModelDto sortModelDto)
        {
            if (sortModelDto.MarkName != String.Empty)
            {
                // Get current Salon
                var salon = Uow.SalonRepository.GetSalonByName(sortModelDto.SalonName);

                var modelsNameCollection = Uow.ModelRepository.GetAll().
                    Where(p => p.Mark.MarkName == sortModelDto.MarkName).
                    Select(p => p.ModelName).Distinct().
                    ToList();

                // If was chose Model
                if (sortModelDto.ModelName != String.Empty)
                {
                    // List All Salons which relevant Model name
                    var modelsWithSort = Uow.ModelRepository.GetAll()
                        .Where(p => p.ModelName == sortModelDto.ModelName).
                        ToList();

                    var modelExistMark = salon.Models.
                        Where(s => s.ModelName == sortModelDto.ModelName).ToList();

                    var relevantModel = modelsWithSort.Except(modelExistMark).ToList();

                    return GetAvailableSortModelDto(relevantModel, salon, modelsNameCollection);
                }

                // Get for all relevant Salons
                var salonsModels = salon.Models.
                    Where(p => p.Mark.MarkName == sortModelDto.MarkName).ToList();

                var allModels = Uow.ModelRepository.GetAll().
                    Where(p => p.Mark.MarkName == sortModelDto.MarkName).ToList();

                // Exclude intersect data
                var modelsAvailable = allModels.Except(salonsModels).ToList();

                // Models rilated with Mark

                return GetAvailableSortModelDto(modelsAvailable, salon, modelsNameCollection);
            }
            else
            {
                throw new Exception();
            }
        }


        private SalonInfoCollectionDto GetAvailableSortModelDto(IEnumerable<Model> models,
            Salon salon, List<string> modelsNameCollection)
        {
            if (salon != null)
            {
                var sortData = new SalonInfoCollectionDto()
                {
                    CollectionSalonInfoDto = models.Select(model => new SalonInfoDto()
                    {
                        SalonInfoId = salon.SalonId,
                        SalonInfoColor = EnumHelpers.GetStringColor(model.Color),
                        SalonInfoModelName = model.ModelName,
                        SalonInfoMarkName = model.Mark.MarkName,
                        SalonInfoPrice = model.Price
                    }),
                    CollectionModelName = modelsNameCollection,
                    CollectionMarkName = Uow.MarkRepository.GetAllMarksName()
                };
                return sortData;
            }
            else
            {
                throw new NullReferenceException();
            }
        }
    }
}
