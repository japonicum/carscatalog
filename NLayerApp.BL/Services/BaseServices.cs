﻿using NLayerApp.DLA.UnitOfWork;

namespace NLayerApp.BL.Services
{
    public abstract class BaseServices
    {
        protected IUnitOfWork Uow { get; set; }

        protected BaseServices(IUnitOfWork uow)
        {
            Uow = uow;
        }
    }
}
