﻿using System;
using System.Linq;
using NLayerApp.BL.DTO.SalonDtos;
using NLayerApp.BL.IServices;
using NLayerApp.BL.Services.ServiceResults;
using NLayerApp.DLA.CustomExceptions;
using NLayerApp.DLA.Entities;
using NLayerApp.DLA.UnitOfWork;

namespace NLayerApp.BL.Services
{
    public class SalonServices : BaseServices, ISalonServices
    {
        public SalonServices(IUnitOfWork uow) : base(uow)
        {
        }

        public SalonInfoCollectionDto GetAllSalonsDto()
        {
            var salons = Uow.SalonRepository.GetAll();

            var s = new SalonInfoCollectionDto
            {
                CollectionSalonDto = salons.Select(salon => new SalonDto()
                {
                    SalonId = salon.SalonId,
                    SalonName = salon.SalonName,
                    OwnerName = salon.OwnerName,
                    Adress = salon.SalonAdress,
                })
            };
            return s;
        }

        public SalonDto GetSalonByIdDto(int id)
        {
            var salon = Uow.SalonRepository.GetById(id);

            return new SalonDto()
            {
                SalonId = salon.SalonId,
                SalonName = salon.SalonName,
                OwnerName = salon.OwnerName,
                Adress = salon.SalonAdress
            };
        }

        public ServiceResult UpdateSalonDto(SalonDto salonDto)
        {

            Salon salon = Uow.SalonRepository.GetById(salonDto.SalonId);
            ServiceData<Salon> result = new ServiceData<Salon>()
            {
                Data = new Salon(), Error = "", Success = true,
            };
            try
            {
                salon.OwnerName = salonDto.OwnerName;
                salon.SalonAdress = salonDto.Adress;
                salon.SalonName = salonDto.SalonName;

                Uow.SalonRepository.Update(salon);

                Uow.SaveChanges();
                result.Data = salon;
            }
            catch (CustomObjectNotFoundException e)
            {
                result.Error = e.Message;
                result.Success = false;
               
            }
            return result;
        }

        public ServiceResult AddSalonDto(SalonDto salonDto)
        {
            var result = new ServiceResult() { Error = "Well", Success = true };

            try
            {
                Salon salon = new Salon()
                {
                    OwnerName = salonDto.OwnerName,
                    SalonAdress = salonDto.Adress,
                    SalonName = salonDto.SalonName,
                };

                Uow.SalonRepository.Create(salon);
                Uow.SaveChanges();
            }
            catch (CustomObjectNotFoundException e)
            {
                result.Error = e.Message;
                result.Success = false;
            }
            catch (Exception e)
            {
                result.Error = e.Message;
                result.Success = false;
            }
            return result;
        }

        public ServiceResult DeleteSalonDto(int id)
        {
            var result = new ServiceResult() { Error = "Well", Success = true };

            try
            {
                Uow.SalonRepository.DeleteSalon(id);
                Uow.SaveChanges();
            }
            catch (CustomObjectNotFoundException e)
            {
                result.Error = e.Message;
                result.Success = false;
            }
            catch (Exception)
            {
                result.Error = "Exception in Salon Service";
                return result;
            }

            return result;
        }
    }
}
