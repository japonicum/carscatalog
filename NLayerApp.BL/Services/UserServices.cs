﻿using NLayerApp.BL.DTO;
using NLayerApp.BL.IServices;
using NLayerApp.DLA.Entities;
using NLayerApp.DLA.UnitOfWork;

namespace NLayerApp.BL.Services
{
    public class UserServices : BaseServices, IUserServices
    {
        public UserServices(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }

        public void AddUser(UserAuthorizetionDto userAuthorizetionDto)
        {
            User user = new User()
            {
                Password = userAuthorizetionDto.Password,
                UserName = userAuthorizetionDto.UserName
            };

            Uow.UserRepository.Create(user);
            Uow.SaveChanges();
        }
    }
}
