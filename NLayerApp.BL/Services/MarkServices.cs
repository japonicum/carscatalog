﻿using NLayerApp.BL.IServices;
using NLayerApp.DLA.UnitOfWork;

namespace NLayerApp.BL.Services
{
    public class MarkServices: BaseServices, IMarkServices
    {
        public MarkServices(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
