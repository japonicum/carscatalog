﻿using System;
using NLayerApp.BL.DTO;
using NLayerApp.BL.IServices;
using NLayerApp.DLA.Entities;
using NLayerApp.DLA.UnitOfWork;

namespace NLayerApp.BL.Services
{
    public class ManufactureServices : BaseServices, IManufactureServices
    {
        public ManufactureServices(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
