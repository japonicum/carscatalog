﻿using System;
using System.Linq;
using Common.Enums;
using NLayerApp.BL.DTO;
using NLayerApp.BL.IServices;
using NLayerApp.DLA.UnitOfWork;
using Common.Helpers;
using NLayerApp.BL.Services.ServiceResults;
using NLayerApp.DLA.CustomExceptions;
using NLayerApp.DLA.Entities;


namespace NLayerApp.BL.Services
{
    public class ModelServices : BaseServices, IModelServices
    {
        public ModelServices(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }

        public InfoModelDto GetAllCarsDto()
        {
            var cars = Uow.ModelRepository.GetAll();

            var s = new InfoModelDto()
            {
                CollectionCarsFullInformationDto = cars.Select(car => new CarsFullInformationDto
                {
                    Id = car.ModelId,
                    ModelName = car.ModelName,
                    MarkName = car.Mark.MarkName,
                    ManufactureName = car.Manufacture.ManufactureName,
                    Price = car.Price,
                    Color = car.Color.ToString(),
                    Weight = car.Weight,
                }),
                CollectionColorsDto = EnumHelpers.GetColorsEnum(),
                CollectionMarksDto = Uow.MarkRepository.GetAllMarksName(),
                CollectionManufacturesDto = Uow.ManufactureRepository.GetAllManufactureName(),
                CollectionModelsDto = Uow.ModelRepository.GetAllModelsNameCollectionString()
            };
            return s;
        }

        public void UpdateCarInformationDto(CarsFullInformationDto carDto)
        {
            Model model = Uow.ModelRepository.GetModelById(carDto.Id);

            Uow.ModelRepository.Update(FillModelFromDto(model, carDto));

            Uow.SaveChanges();
        }

        public Model FillModelFromDto(Model model, CarsFullInformationDto carDto)
        {
            model.Mark = Uow.MarkRepository.GetMarkByName(carDto.MarkName);
            model.Manufacture = Uow.ManufactureRepository.GetManufactureByName(carDto.ManufactureName);
            model.Color = (CarColors)Enum.Parse(typeof(CarColors), carDto.Color);
            model.Weight = carDto.Weight;
            model.Price = carDto.Price;
            model.ModelName = carDto.ModelName;
            model.ManufactureDate = DateTime.MinValue;
            return model;
        }

        public ServiceResult DeleteCarDto(int id)
        {
            var result = new ServiceResult() { Error = "Well", Success = true };

            try
            {
                Uow.ModelRepository.RemoveModel(id);
                Uow.SaveChanges();
            }
            catch (CustomObjectNotFoundException)
            {
                result.Error = "Not found Mark while removing";
                result.Success = false;
            }
            catch (Exception)
            {
                result.Error = "Exception while removing Mark";
                result.Success = false;
            }

            return result;
        }

        public void AddCarDto(CarsFullInformationDto carDto)
        {
            var model = new Model();

            Uow.ModelRepository.Create(FillModelFromDto(model, carDto));

            Uow.SaveChanges();
        }
    }
}
