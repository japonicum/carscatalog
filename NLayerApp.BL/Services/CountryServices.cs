﻿using System.Collections.Generic;
using System.Linq;
using NLayerApp.BL.DTO;
using NLayerApp.BL.IServices;
using NLayerApp.DLA.Entities;
using NLayerApp.DLA.UnitOfWork;

namespace NLayerApp.BL.Services
{
    public class CountryServices : BaseServices, ICountryServices
    {
        public CountryServices(IUnitOfWork unityOfWork) : base(unityOfWork)
        {

        }

        public IList<CountryDto> GetAllCountriesDto()
        {
            IEnumerable<Country> countries = Uow.CountryRepository.GetAll();
            IList<CountryDto> countriesDto = new List<CountryDto>();
            
            var countryDtos = countries.Select(country => new CountryDto()
            {
                Id = country.CountryId,
                CountryName = country.CountryName

            }).ToList();

            return countryDtos;
        }

        public CountryDto GetCountryDtoById(int i)
        {
            Country country = Uow.CountryRepository.GetById(i);

            var countryDto = new CountryDto
            {
                Id = country.CountryId,
                CountryName = country.CountryName
            };
            return countryDto;
        }
    }
}
